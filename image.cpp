//
//  image.cpp
//  Offgrid demonstration source
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <cstdio>
#include <iostream>
#include <algorithm>
#include "image.hpp"

/******************************************************************************/

/// Allocate the image buffer, set the image size
void image::Allocate( size_t width, size_t height )
{
    this->width = width;
    this->height = height;
    
    delete [] this->buffer; // just in case it was already allocated
    this->buffer = new uint8_t[ width * height ];
    
    memset( this->buffer, 0, this->width * this->height );
}

/******************************************************************************/

/// Write the image buffer to an 8 bit/channel Portable Gray Map (PGM) file
void image::WritePGM( const char *name )
{
    FILE *outfile = NULL;
    
    // see if we can create or update this filename
    if((outfile=fopen(name,"wb"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", name);
        exit(-1);
        }
    
	// write PGM file header
    fprintf(outfile,"P5\n# CREATOR: offgrid\n%ld %ld\n%d\n",this->width,this->height,255 );
    // write the image buffer
    fwrite( this->buffer, this->width, this->height, outfile );
    // and close the file
    fclose(outfile);
}

/******************************************************************************/

/// Write a pixel value, clipped to image bounds
void image::WritePixel( int64_t x, int64_t y, uint8_t value )
{
    if (x >= 0 && x < this->width && y >= 0 && y < this->height)
        buffer[ y*this->width + x ] = value;
}

/******************************************************************************/

/// Read a pixel value, clipped to image bounds
uint8_t image::ReadPixel( int64_t x, int64_t y )
{
    if (x >= 0 && x < this->width && y >= 0 && y < this->height)
        return buffer[ y*this->width + x ];
    else
        return 0;
}

/******************************************************************************/

/// Fill specified rectangular area with value, after clipping to image bounds
void image::FillRectClipped( int64_t top, int64_t left, int64_t bottom, int64_t right, uint8_t value )
{
    top = std::max( top, (int64_t)0 );
    left = std::max( left, (int64_t)0 );
    bottom = std::min( bottom, (int64_t)(this->height) );
    right = std::min( right, (int64_t)(this->width) );

    for (int64_t y = top; y < bottom; ++y)
        for (int64_t x = left; x < right; ++x)
            this->buffer[ y * this->width + x ] = value;
}

/******************************************************************************/
/******************************************************************************/
