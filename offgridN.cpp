//
//  offgridN.cpp
//      Offgrid demonstration source
//
//  Created by Chris Cox on July 8, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <cstdio>
#include <cmath>
#include <algorithm>
#include <array>
#include <vector>
#include "hash.hpp"
#include "image.hpp"
#include "offgridN.hpp"


/******************************************************************************/

template< size_t N >
boxFloat Box3FromN( const boxN<double,N> &input ) {
    static_assert( N >= 3 );
    boxFloat out( input.min[0], input.min[1], input.min[2],
                  input.max[0], input.max[1], input.max[2] );
    return out;
}

// special case where no third entry exists
template<>
boxFloat Box3FromN( const boxN<double,2> &input ) {
    boxFloat out( input.min[0], input.min[1], 0.0,
                  input.max[0], input.max[1], 1.0 );
    return out;
}

// special case where no second or third entry exists
template<>
boxFloat Box3FromN( const boxN<double,1> &input ) {
    boxFloat out( input.min[0], 0.0, 0.0,
                  input.max[0], 1.0, 1.0 );
    return out;
}

/******************************************************************************/

// for debugging - to show orthogonal rotations with N >= 4
template< size_t N >
boxFloat Box3FromN_Hack( const boxN<double,N> &input, size_t Kout, size_t Min ) {
    static_assert( N >= 4 );
    assert( Kout <= 2 );
    assert( Min < N );
    
    boxFloat out;
    
    switch (Kout)
        {
        case 0:
            out = boxFloat( input.min[Min], input.min[1], input.min[2],
                            input.max[Min], input.max[1], input.max[2] );
            break;
        case 1:
            out = boxFloat( input.min[0], input.min[Min], input.min[2],
                            input.max[0], input.max[Min], input.max[2] );
            break;
        case 2:
            out = boxFloat( input.min[0], input.min[1], input.min[Min],
                            input.max[0], input.max[1], input.max[Min] );
            break;
        default:
            assert(false);
            break;
        }
    
    return out;
}

/******************************************************************************/
/******************************************************************************/

FILE *OpenX3D( const char *name, unsigned N )
{
    FILE *out = NULL;
    
    // see if we can create or update this filename
    if((out=fopen(name,"w"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", name);
        exit(-1);
        }
    
	fprintf( out, "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
	fprintf( out, "<!DOCTYPE X3D PUBLIC \"ISO//Web3D//DTD X3D 3.0//EN\" \"http://www.web3d.org/specifications/x3d-3.0.dtd\">\n");
	fprintf( out, "<X3D profile='Interchange' version='3.0' xmlns:xsd='http://www.w3.org/2001/XMLSchema-instance' xsd:noNamespaceSchemaLocation =' http://www.web3d.org/specifications/x3d-3.0.xsd '>\n");
	fprintf( out, "<head>\n");
	fprintf( out, "<meta name='title' content='offgridN'/>\n");
	fprintf( out, "<meta name='description' content='OffgridN boxes with %u dimensions'/>\n", N );
	fprintf( out, "<meta name='generator' content='OffgridN'/>\n");
	fprintf( out, "</head>\n");
	fprintf( out, "<Scene>\n");
	fprintf( out, "<Transform scale='%f %f %f'>\n", 1.0,  1.0,  1.0);
 
    return out;
}

/******************************************************************************/

void CloseX3D( FILE *out )
{
	fprintf( out, "</Transform>\n</Scene>\n</X3D>\n");
    fclose(out);
}

/******************************************************************************/

const
point3Float red ( 1.0, 0.2, 0.2 );

const
point3Float blue ( 0.1, 0.1, 0.8 );

/******************************************************************************/

void WriteX3DBox( const boxFloat &box, const point3Int &offset, const point3Float &color, FILE *out )
{   // X3D boxes are defined from center
    fprintf( out, "<Transform translation='%f %f %f'>\n", (box.xmin+box.xmax)/2+offset.x, (box.ymin+box.ymax)/2+offset.y, (box.zmin+box.zmax)/2+offset.z );
    fprintf( out, "<Shape>\n");
    fprintf( out, "<Appearance>\n");
    fprintf( out, "<Material diffuseColor='%.2f %.2f %.2f' shininess='%.2f' transparency='%.2f'/>\n", color.x, color.y, color.z, 0.0, 0.50 );
    fprintf( out, "</Appearance> \n");
    fprintf( out, "<Box size='%f %f %f'/> \n", (box.xmax - box.xmin), (box.ymax - box.ymin), (box.zmax - box.zmin) );
    fprintf( out, "</Shape>\n");
    fprintf( out, "</Transform>\n");
}

/******************************************************************************/
/******************************************************************************/

template< typename T, size_t N >
T vector_sum( const vecN<T,N> &vec )
{
    T sum = T(0);
    for ( const auto &v : vec )
        sum += v;
    return sum;
}

/******************************************************************************/

// All boxes will be between (2*edge) and (2.0-2*edge) in dimension
//      with average size 1.0.
template< size_t N >
double box_randomN( const vecN<int64_t,N> &vec, int64_t seed )
{
    const double edge = 0.1;        // could be a parameter, range 0 ... 0.5
    const double range = 1.0 - 2.0 * edge;
    
    int64_t hashed = hash64<N>( vec, seed );
    double random = hash_to_double( hashed );
    double result = edge + range * random;
    return result;
}

/******************************************************************************/

/*

1D is trivial, swapping min and max roles
    X X X X

2D is fairly easy
    0 1
    2 3

    X Y X Y
    Y X Y X
    X Y X Y

0 X   00
1 Y   10
2 Y'  01 = Y ^ 11
3 X'  11 = X ^ 11



To define an N dimensional box, we need 2*N values (min and max for each dimension).
Key color/point is the current point index with zero offsets.



N colors == N dimensions (it's just easier to visualize as colors)
Key color is determined by ((sum of indices) % N) == an abstracted N dimensional checkerboard.
Each color corresponds to a limit in one dimension.

N colors/dimensions with a 2^N search block requires:
1) Must use exactly 2 of each N colors per block (min and max values) to define a box.
2) There are 2^N total values per block, of N colors (in 3D: 8 values, 3 colors, 2 values must be unused, and there are only 2 of the key color).
3) Moving plus or minus one on any axis must reuse the new key point in all directions to preserve geometry.
4) Moving plus one on any axis means the key color increases by 1 modulo N (which means that N of the next colors be used! N > 2 not possible because of #1).
5) Moving minus one on any axis means the key color decreases by 1 modulo N (which means that N of the previous colors be used! N > 2 not possible because of #1).
6) Moving plus or minus on any axis must reuse exactly N values (min or max) to preserve geometry.
    In 3D, that means an L shape in all directions, which means 2 diagonal values are not used -- which also violates #3.

Even worse,
N colors/dimensions with a N^N search block requires:
1) Must use exactly 2 of each N colors per block (min and max values for each axis) to define a box.
2) There are N^N total values per block, of N colors (in 3D: 27 values, 3 colors, 9 of each color).
3) Moving plus or minus one on any axis must reuse the new key point in all directions.
4) Moving plus one on any axis means the key color increases by 1 modulo N (which means that N of the next colors be used! N > 2 not possible because of #1).
5) Moving minus one on any axis means the key color decreases by 1 modulo N (which means that N of the previous colors be used! N > 2 not possible because of #1).
6) Moving plus or minus on any axis must reuse exactly N values (min or max) to preserve geometry -- not possible.

So, simple N coloring won't work beyond 2 dimensions. Darnit.



IDEA - diagonal corners per axis, really 4 colors in 3D, but not as simple a pattern
    2^N block, using 2*N values, some values unused for N=3 and above
    2^N/2 = 2^(N-1) diagonals available, which is >= N for N=1 and above
    N=1 is possible, but trivial  (eh, try to make the code work for it for completeness)
    Does not use all key values, but has values defined for all positions.
    min coordinate sets min limit, max coordinate sets max limit
    in 3D, Z odd is Z even with 2x2 block rotated 180 degrees, or 1 diagonal index out of phase
    diagonal offset as bitfield... opposite corner is bits inverted


3D
    0 1     4 5
    2 3     6 7

    X Y X Y     Z 0 Z 0         color 0 1   3 2
    0 Z 0 Z     Y X Y X               2 3   1 0
    X Y X Y     Z 0 Z 0

0 X     000
1 Y     100
2 0     010
3 Z     110
4 Z'    001 = Z ^ 111
5 0'    101 = 0 ^ 111
6 Y'    011 = Y ^ 111
7 X'    111 = X ^ 111

-+x = coords ^ 100
-+y = coords ^ 010
+-z = coords ^ 001  and color = 3 - (index&3)

down, right, and back
origin
    X Y     Z 0     XMin Ymin   Zmax 0
    0 Z     Y X     0    Zmin   Ymax XMax

down and back
right
    Y X     0 Z     Ymin  XMax  0    Zmax
    Z 0     X Y     Zmin  0     Xmin Ymax

right and back
down
    0 Z     Y X     0    Zmin   Ymin Xmax
    X Y     Z 0     Xmin YMax   Zmax 0

down and right
back
    Z 0     X Y     Zmin 0      Xmin Ymin
    Y X     0 Z     Ymax Xmax   0    Zmax



4D
    0 1     4 5     8 9     C D
    2 3     6 7     A B     E F

    X Y     1 2     I 3     Z 0
    0 Z     3 I     2 1     Y X
    
    X Y X Y     1 2 1 2     I 3 I 3     Z 0 Z 0
    0 Z 0 Z     3 I 3 I     2 1 2 1     Y X Y X
    X Y X Y     1 2 1 2     I 3 I 3     Z 0 Z 0

-+x = coords ^ 1000
-+y = coords ^ 0100
+-z = coords ^ 0010
+-I = coords ^ 0001 and color = 7 - (index&7)

0 X     0000
1 Y     1000
2 0     0100
3 Z     1100
4 1     0010
5 2     1010
6 3     0110
7 I     1110
8 I'    0001 = I ^ 1111
9 3'    1001 = 3 ^ 1111
A 2'    0101 = 2 ^ 1111
B 1'    1101 = 1 ^ 1111
C Z'    0011 = Z ^ 1111
D 0'    1011 = 0 ^ 1111
E Y'    0111 = Y ^ 1111
F X'    1111 = X ^ 1111

*/

/******************************************************************************/

template< typename T, size_t N >
size_t vector_grid_color( const vecN<T,N> &vec )
{
    static_assert( N >= 1 );
    static_assert( N < (8*sizeof(size_t)) );
    
    size_t sum = 0;
    const size_t diagonals = (1ULL<<(N-1));     // total number of diagonals 1,2,4,8,16,etc.

    for (size_t i = 0; i < (N-1); ++i)
        {
        if ((vec[i] & 1) != 0)
            sum += (1ULL << i);
        }
    
    if ((vec[(N-1)] & 1) != 0)
        sum = (diagonals-1) - sum;
    
    assert( sum < diagonals );
    
    return sum;
}

/******************************************************************************/

// Return the irregular box for which this integer grid cell contains the minimum coordinates
template< size_t N >
auto OffgridN_CellToBox( const vecN<int64_t,N> &input, int64_t seed )
{
    static_assert( N >= 1 );
    static_assert( N < (8*sizeof(size_t)) );
    
    using vecInt = vecN<int64_t, N>;
    
    boxN<double,N> resultBox;
    
    // color ranges 0 ... (2^(N-1))-1 == index of diagonals (zero based, of course)
    size_t color = vector_grid_color<int64_t,N>( input );
    
    for (size_t d = 0; d < N; ++d)
        {
        vecInt offsetMin;
        vecInt offsetMax;
        
        size_t baseBitPattern = (1ULL << d)-1;
        
        for (size_t k = 0; k < N; ++k)
            {
            // set base offset for each output channel
            int posBit = (baseBitPattern & (1ULL << k)) != 0;
            offsetMin[k] = posBit;
            offsetMax[k] = (posBit ^ 1);
            
            // adjust for position offset based on color of current position
            int adjustBit = (color & (1ULL << k)) != 0;
            offsetMin[k] ^= adjustBit;
            offsetMax[k] ^= adjustBit;
            }
        
        // Because of position offsets, we could have min and max reversed
        // but min always has it's own channel at zero
        // and max always has it's own channel at one
        // So test and swap as needed
        if (offsetMin[d] != 0)
            std::swap(offsetMin, offsetMax);
        
        // make sure we got the min and max coordinates right
        assert( offsetMin[d] == 0 );
        assert( offsetMax[d] == 1 );
        
        vecInt tempDmin = input + offsetMin;
        vecInt tempDmax = input + offsetMax;
        
        resultBox.min[d] = input[d] + box_randomN<N>( tempDmin, seed );
        resultBox.max[d] = input[d] + box_randomN<N>( tempDmax, seed ) + 1;
        }

    assert( resultBox.isValid() );
    
    return resultBox;
}

/******************************************************************************/

// Return the integer grid cell for which the minimum coordinate box contains this point
template< size_t N >
vecN<int64_t, N> OffgridN_PointToCell( const vecN<double,N> &input, int64_t seed )
{
    static_assert( N >= 1 );
    static_assert( N < (8*sizeof(size_t)) );
    
    using vecInt = vecN<int64_t, N>;
    using boxDouble = boxN<double,N>;
    
    vecInt baseCell = FloorPoint( input );
    
    vecInt resultCell = baseCell;
    bool foundCell = false;

/*
    The base cell contains a rect with minimum coordinates (goes to + coordinates)
    Any point in the base cell is contained by rects from above or beside the base.
        So +1 offsets can't contain the point.
    Only consider -1 and 0 offsets -- aka binary iteration subtracting values
    Still must test up to 2^N cells per point, ouch!
    Works in 2D, fails in 3D


// TODO -
    There must be somw way to binary search this...
    easy in 2D, but 3D and above have empty cells
        Can I just change coordinates bbased on < min[d] ?
    Nope

*/
#if 0
// seems to work for 2D, but can't really prove that it always works.
// 2N
// FAILS in 3D!
    
    vecInt testCell = baseCell;
    boxDouble boxBase = OffgridN_CellToBox<N>( testCell, seed );
    
    boxDouble boxN = boxBase;
    if ( boxBase.ContainsPoint( input ) )
        {
        foundCell = true;
        resultCell = testCell;
        }
    
    for (size_t k = 0; k < N && !foundCell; ++k)
        {
        testCell[k] -= (input[k] < boxN.min[k]) ? 1 : 0;
        
        boxN = OffgridN_CellToBox<N>( testCell, seed );
        if ( boxN.ContainsPoint( input ) )
            {
            foundCell = true;
            resultCell = testCell;
            break;
            }
        
        testCell[k] += (input[k] >= boxN.max[k]) ? 1 : 0;
        
        boxN = OffgridN_CellToBox<N>( testCell, seed );
        if ( boxN.ContainsPoint( input ) )
            {
            foundCell = true;
            resultCell = testCell;
            break;
            }
        }
    
    for (size_t k = 0; k < N && !foundCell; ++k)
        {
        testCell[k] += (input[k] >= boxN.max[k]) ? 1 : 0;
        
        boxN = OffgridN_CellToBox<N>( testCell, seed );
        if ( boxN.ContainsPoint( input ) )
            {
            foundCell = true;
            resultCell = testCell;
            break;
            }
        
        testCell[k] -= (input[k] < boxN.min[k]) ? 1 : 0;
        
        boxN = OffgridN_CellToBox<N>( testCell, seed );
        if ( boxN.ContainsPoint( input ) )
            {
            foundCell = true;
            resultCell = testCell;
            break;
            }
        }

#elif 0
//(2*B+1)^N         still fails for some points in 3D and above!
    const int64_t bound = 5;
    for (int64_t xx = -bound; xx <= bound; ++xx )
        {
        for (int64_t yy = -bound; yy <= bound; ++yy )
            {
            for (int64_t zz = -bound; zz <= bound; ++zz )
                {
                vecInt testCell = baseCell;
                
                testCell[0] += xx;
                testCell[1] += yy;
                testCell[2] += zz;
                
                boxDouble boxN = OffgridN_CellToBox<N>( testCell, seed );
                if ( boxN.ContainsPoint( input ) )
                    return testCell;
                }
            }
        }

#else
// 2^N
// FAILS for 3D -- how?  I guess the upward assumption isn't correct somewhere?
// No, upward/lower assumption must hold.  So how am I missing cells that could hold the point?

    size_t limit = (1ULL << N);
    
    for (size_t offset = 0; offset < limit; ++offset)
        {
        vecInt testCell = baseCell;
        
        for (size_t k = 0; k < N; ++k)
            {
            int offBit = (offset & (1ULL << k)) != 0;
            testCell[k] -= offBit;
            }
        
        boxDouble boxN = OffgridN_CellToBox<N>( testCell, seed );
        if ( boxN.ContainsPoint( input ) )
            {
            foundCell = true;
            resultCell = testCell;
            break;
            }
        }

#endif
    
    // could use the box for shaders (subdivide, assign greeble shape, etc.)
    
    // make sure we actually found a containing rect!
    assert( foundCell );
    
    return resultCell;
}

/******************************************************************************/
/******************************************************************************/

// convenience wrapper to output an image
boxN<double,2> OffgridN_CellToBox2( const vecN<int64_t,2> &input, int64_t seed )
{
    return OffgridN_CellToBox<2>( input, seed );
}
/******************************************************************************/

// convenience wrapper to output an image
vecN<int64_t,2> OffgridN_PointToCell2( const vecN<double,2> &input, int64_t seed )
{
    return OffgridN_PointToCell<2>( input, seed );
}

/******************************************************************************/

// Iterate the integer grid with N dimensional logic to create irregular box coordinates
// output an X3D file with 3D boxes
void OffgridN_Test1( const size_t width, const int64_t seed, const char *name )
{
    FILE *out = OpenX3D( name, 1 );
    const int64_t spacing = (width + 4);  // plus 2 for extents, plus 1 for spacing between cubes
    
    for( int64_t x = 0; x < width; ++x ) {
        
        vecN<int64_t,1> pt = { x };
        boxN<double,1> outBox = OffgridN_CellToBox( pt, seed );
        boxFloat box3 = Box3FromN( outBox );
        bool odd = ((vector_sum(pt) & 1) != 0);
        point3Int offset( 0,0,0 );  // (i, j, k)
        WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
        
    }   // for x

    CloseX3D( out );
}

/******************************************************************************/

// Iterate the integer grid with N dimensional logic to create irregular box coordinates
// output an X3D file with 3D boxes
void OffgridN_Test2( const size_t width, const int64_t seed, const char *name )
{
    FILE *out = OpenX3D( name, 2);
    const int64_t spacing = (width + 4);  // plus 2 for extents, plus 1 for spacing between cubes
    
    for( int64_t y = 0; y < width; ++y ) {
        for( int64_t x = 0; x < width; ++x ) {
            
            vecN<int64_t,2> pt = { x, y };
            boxN<double,2> outBox = OffgridN_CellToBox( pt, seed );
            boxFloat box3 = Box3FromN( outBox );
            bool odd = ((vector_sum(pt) & 1) != 0);
            point3Int offset( 0,0,0 );  // (i, j, k)
            WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
            
        }   // for x
    }   // for y

    CloseX3D( out );
}

/******************************************************************************/

// Iterate the integer grid with N dimensional logic to create irregular box coordinates
// output an X3D file with 3D boxes
void OffgridN_Test3( const size_t width, const int64_t seed, const char *name )
{
    FILE *out = OpenX3D( name, 3 );
    const int64_t spacing = (width + 4);  // plus 2 for extents, plus 1 for spacing between cubes

    for( int64_t z = 0; z < width; ++z ) {
        for( int64_t y = 0; y < width; ++y ) {
            for( int64_t x = 0; x < width; ++x ) {
                
                vecN<int64_t,3> pt = { x, y, z };
                boxN<double,3> outBox = OffgridN_CellToBox( pt, seed );
                boxFloat box3 = Box3FromN( outBox );
                bool odd = ((vector_sum(pt) & 1) != 0);
                point3Int offset( 0,0,0 );  // (i, j, k)
                WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
            
            }   // for x
        }   // for y
    }   // for z

    CloseX3D( out );
}

/******************************************************************************/

// Iterate the integer grid with N dimensional logic to create irregular box coordinates
// output an X3D file with 3D boxes
void OffgridN_Test4( const size_t width, const int64_t seed, const char *name )
{
    FILE *out = OpenX3D( name, 4 );
    const int64_t spacing = (width + 4);  // plus 2 for extents, plus 1 for spacing between cubes

    for( int64_t i = 0; i < width; ++i ) {
        for( int64_t z = 0; z < width; ++z ) {
            for( int64_t y = 0; y < width; ++y ) {
                for( int64_t x = 0; x < width; ++x ) {
                    
                    vecN<int64_t,4> pt = { x, y, z, i };
                    boxN<double,4> outBox = OffgridN_CellToBox( pt, seed );
                    boxFloat box3 = Box3FromN( outBox );
                    bool odd = ((vector_sum(pt) & 1) != 0);
                    point3Int offset( i,0,0 );  // (i, j, k)
                    WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
                    
                }   // for x
            }   // for y
        }   // for z
    }   // for i


#if DEBUG
    // show all orthogonal rotations offset vertically
    //   (IYZ+X) (XIZ+Y) (XYI+Z)
    for (size_t d = 0; d < 3; ++d) {
        for( int64_t i = 0; i < width; ++i ) {
            for( int64_t z = 0; z < width; ++z ) {
                for( int64_t y = 0; y < width; ++y ) {
                    for( int64_t x = 0; x < width; ++x ) {
                        
                        vecN<int64_t,4> pt = { x, y, z, i };
                        boxN<double,4> outBox = OffgridN_CellToBox( pt, seed );
                        boxFloat box3 = Box3FromN_Hack( outBox, d, 3 );
                        bool odd = ((vector_sum(pt) & 1) != 0);
                        point3Int offset( pt[d], d+1, 0 );  // (i, j, k)
                        WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
                        
                    }   // for x
                }   // for y
            }   // for z
        }   // for i
    }
#endif

    CloseX3D( out );
}

/******************************************************************************/

// Iterate the integer grid with N dimensional logic to create irregular box coordinates
// output an X3D file with 3D boxes
void OffgridN_Test5( const size_t width, const int64_t seed, const char *name )
{
    FILE *out = OpenX3D( name, 5 );
    const int64_t spacing = (width + 4);  // plus 2 for extents, plus 1 for spacing between cubes

    for( int64_t j = 0; j < width; ++j ) {
        for( int64_t i = 0; i < width; ++i ) {
            for( int64_t z = 0; z < width; ++z ) {
                for( int64_t y = 0; y < width; ++y ) {
                    for( int64_t x = 0; x < width; ++x ) {
                        
                        vecN<int64_t,5> pt = { x, y, z, i, j };
                        boxN<double,5> outBox = OffgridN_CellToBox( pt, seed );
                        boxFloat box3 = Box3FromN( outBox );
                        bool odd = ((vector_sum(pt) & 1) != 0);
                        point3Int offset( i,j,0 );  // (i, j, k)
                        WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
                        
                    }   // for x
                }   // for y
            }   // for z
        }   // for i
    } // for j

#if DEBUG
    // show some orthogonal rotations offset horizontally
    for (size_t d = 0; d < 3; ++d) {
        for( int64_t j = 0; j < width; ++j ) {
            for( int64_t i = 0; i < width; ++i ) {
                for( int64_t z = 0; z < width; ++z ) {
                    for( int64_t y = 0; y < width; ++y ) {
                        for( int64_t x = 0; x < width; ++x ) {
                                
                                vecN<int64_t,5> pt = { x, y, z, i, j };
                                boxN<double,5> outBox = OffgridN_CellToBox( pt, seed );
                                boxFloat box3 = Box3FromN_Hack( outBox, d, 4 );
                                bool odd = ((vector_sum(pt) & 1) != 0);
                                point3Int offset( i, pt[d], d+1 );  // (i, j, k)
                                WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
                            
                        }   // for x
                    }   // for y
                }   // for z
            }   // for i
        } // for j
    }
#endif

    CloseX3D( out );
}

/******************************************************************************/

// Iterate the integer grid with N dimensional logic to create irregular box coordinates
// output an X3D file with 3D boxes
void OffgridN_Test6( const size_t width, const int64_t seed, const char *name )
{
    FILE *out = OpenX3D( name, 6 );
    const int64_t spacing = (width + 4);  // plus 2 for extents, plus 1 for spacing between cubes

    for( int64_t k = 0; k < width; ++k ) {
        for( int64_t j = 0; j < width; ++j ) {
            for( int64_t i = 0; i < width; ++i ) {
                for( int64_t z = 0; z < width; ++z ) {
                    for( int64_t y = 0; y < width; ++y ) {
                        for( int64_t x = 0; x < width; ++x ) {
                            
                            vecN<int64_t,6> pt = { x, y, z, i, j, k };
                            boxN<double,6> outBox = OffgridN_CellToBox( pt, seed );
                            boxFloat box3 = Box3FromN( outBox );
                            bool odd = ((vector_sum(pt) & 1) != 0);
                            point3Int offset( i,j,k );  // (i, j, k)
                            WriteX3DBox( box3, offset * spacing, odd ? red : blue, out );
                            
                        } // for x
                    } // for y
                } // for z
            } // for i
        } // for j
    } // for k

    CloseX3D( out );
}

/******************************************************************************/

// return a number between -range and +range
double
hashRange( const int64_t x, const int64_t seed, const double range )
{
    int64_t val = hash64( hash64(x) ^ seed );
    double result = (2.0 * range * hash_to_double( val )) - range;
    return result;
}

/******************************************************************************/

template< size_t N >
void TestPointToCellN_inner( const size_t limit, const int64_t seed, const double range )
{
    using vecInt = vecN<int64_t, N>;
    using vecDouble = vecN<double, N>;

#if 1
    for (size_t i = 0; i < limit; ++i)
        {
        vecDouble testPoint;
        for (size_t k = 0; k < N; ++k)
            testPoint[k] = hashRange( i+k, seed, range );
        (void) OffgridN_PointToCell<N>( testPoint, seed );
        }
#else
/*
First 3D fail

input
[0]	double	-3.4877198669013758
[1]	double	15.369714602241025
[2]	double	30.6319316612854
reproducible!
input = { -3.48, 15.37, 30.63 };

baseCell	vecInt
[0]	long long	-4
[1]	long long	15
[2]	long long	30

limit = 8

foundCell = false

baseCell    0x0
min
[0]	double	-3.5663552534453156   inside
[1]	double	15.103857111189381    inside
[2]	double	30.73492129727725       <
max
[0]	double	-2.1291789032790613
[1]	double	16.846228481779086
[2]	double	31.118967513757728

should be -Z -Y     0x06
Still fails with reduced precision input - so precision is not the problem

-X      0x01
min
[0]	double	-4.7453864811411002
[1]	double	15.103857111189381      inside
[2]	double	30.148860600636212
max
[0]	double	-3.5663552534453151     >
[1]	double	16.174438358912653
[2]	double	31.118967513757728      >

-Y      0x02
min
[0]	double	-3.4676992844489396     <
[1]	double	14.772428069144887
[2]	double	30.73492129727725
max
[0]	double	-2.1291789032790613
[1]	double	15.103857111189381      >
[2]	double	31.818476617260348

-X -Y   0x03
min
[0]	double	-4.7453864811411002     inside
[1]	double	14.600263756979503
[2]	double	30.148860600636212      inside
max
[0]	double	-3.4676992844489396
[1]	double	15.103857111189381      >
[2]	double	31.818476617260348

-Z      0x04
min
[0]	double	-3.5663552534453156     inside
[1]	double	15.710919466438629      <
[2]	double	29.377687158694201      inside
max
[0]	double	-2.8939676813725081
[1]	double	16.846228481779086
[2]	double	30.73492129727725

-Z -X       0x05
boxN
[0]	double	-4.8514817766393366
[1]	double	15.710919466438629      <
[2]	double	29.377687158694201
max
[0]	double	-3.5663552534453151     >
[1]	double	16.174438358912653
[2]	double	30.148860600636212      >

-Z -Y       0x06
min
[0]	double	-3.4676992844489396     <
[1]	double	14.772428069144887      inside
[2]	double	29.246963735061662      inside
max
[0]	double	-2.8939676813725081
[1]	double	15.710919466438629
[2]	double	30.73492129727725

-Z -Y -X    0x07
min
[0]	double	-4.8514817766393366     inside
[1]	double	14.600263756979503      inside
[2]	double	29.246963735061662
max
[0]	double	-3.4676992844489396
[1]	double	15.710919466438629
[2]	double	30.148860600636212      >



Searching +- 10 STILL FAILS
WTAF?

Random points also continue to fail.
HOW? Are there gaps that I'm not seeing in 3D?



Positive fail
input
[0]	double	26.211343074414266
[1]	double	5.4089165380307627
[2]	double	14.790348066753101


*/
        vecDouble testPoint = { -3.48, 15.37, 30.63 };
        (void) OffgridN_PointToCell<N>( testPoint, seed );
#endif

}
/******************************************************************************/

// generate random points and call PointToCell
void TestPointToCellN()
{
    double range = 32.0;
    const size_t limit = 1000;
    const int64_t seed = hash64(int64_t(0x875020709));
    
//    TestPointToCellN_inner<1>( limit, seed, range );  // works
//    TestPointToCellN_inner<2>( limit, seed, range );  // works
    TestPointToCellN_inner<3>( limit, seed, range );    // FAIL
//    TestPointToCellN_inner<4>( limit, seed, range );  // FAIL
}

/******************************************************************************/
/******************************************************************************/
