//
//  offgrid.cpp
//  Offgrid demonstration source
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <cstdio>
#include <cmath>
#include <algorithm>
#include "hash.hpp"
#include "image.hpp"
#include "offgrid.hpp"

/******************************************************************************/
/******************************************************************************/

// All boxes will be between (2*edge) and (2.0-2*edge) in dimension
//      with average size 1.0.
// To make the boxes wrap around seamlessly, make this hash function wrap on an even number of integer points.
//      It must be even to preserve the checkerboard ordering.
double box_random( int64_t x, int64_t y, int64_t seed )
{
    const double edge = 0.1;        // could be a parameter, range 0..0.5
    const double range = 1.0 - 2.0 * edge;
    
    double random = hashXY_float( x,y,seed );
    double result = edge + range * random;
    
    return result;
}

/******************************************************************************/

// Return the irregular rect for which this integer grid cell contains the top left
rectFloat Offgrid_CellToRect( int64_t ix, int64_t iy, int64_t seed )
{
    // checkerboard even and odd, vertical and horizontal limits
    bool even = ((ix ^ iy) & 0x01) == 0;       // checkerboard pattern
    
    double fx = ix;
    double fy = iy;
    
    double horiz_limit, vert_limit, horiz2_limit, vert2_limit;

    if (even)
        {
        horiz_limit = fx + box_random(ix,iy,seed);
        vert_limit = fy + box_random(ix+1,iy,seed);
        vert2_limit = fy + box_random(ix,iy+1,seed) + 1;
        horiz2_limit = fx + box_random(ix+1,iy+1,seed) + 1;
        }
    else
        {
        vert_limit = fy + box_random(ix,iy,seed);
        horiz_limit = fx + box_random(ix,iy+1,seed);
        horiz2_limit = fx + box_random(ix+1,iy,seed) + 1;
        vert2_limit = fy + box_random(ix+1,iy+1,seed) + 1;
        }

    // here, the limits are always in known order
    double left = horiz_limit;
    double right = horiz2_limit;
    double top = vert_limit;
    double bottom = vert2_limit;

    assert( left <= right );
    assert( top <= bottom );
    
    return rectFloat( top, left, bottom, right );
}

/******************************************************************************/

// For a given global coordinate, determine which integer cell it belongs to.
pointInt Offgrid_PointToCell( double x, double y, int64_t seed )
{
    double fx = floor(x);
    double fy = floor(y);
    
    int64_t ix = (int64_t)fx;
    int64_t iy = (int64_t)fy;

    // checkerboard even and odd, vertical and horizontal limits
    bool even = ((ix ^ iy) & 0x01) == 0;       // checkerboard pattern
    
    double horiz_limit, vert_limit, horiz2_limit, vert2_limit;
    int64_t xoff, yoff;

    if (even)
        {
        horiz_limit = fx + box_random(ix,iy,seed);
        xoff = (x <= horiz_limit) ? -1 : +1;
        
        vert_limit = fy + box_random(ix+xoff,iy,seed);
        yoff = (y <= vert_limit) ? -1 : +1;
        
        vert2_limit = fy + box_random(ix,iy+yoff,seed) + yoff;
        horiz2_limit = fx + box_random(ix+xoff,iy+yoff,seed) + xoff;
        }
    else
        {
        vert_limit = fy + box_random(ix,iy,seed);
        yoff = (y <= vert_limit) ? -1 : +1;
        
        horiz_limit = fx + box_random(ix,iy+yoff,seed);
        xoff = (x <= horiz_limit) ? -1 : +1;
        
        horiz2_limit = fx + box_random(ix+xoff,iy,seed) + xoff;
        vert2_limit = fy + box_random(ix+xoff,iy+yoff,seed) + yoff;
        }

    // Limits can be swapped depending on starting point, so we need to sort them
    // The bounds can be used for shading, or subdivision for greeble
    double left = std::min(horiz_limit, horiz2_limit);
    double right = std::max(horiz_limit, horiz2_limit);
    double top = std::min(vert_limit, vert2_limit);
    double bottom = std::max(vert_limit, vert2_limit);

    assert( x <= right );
    assert( x >= left );
    assert( y >= top );
    assert( y <= bottom );

    // Each integer grid square contains 4 corners of 4 rectangles.
    // But one specific corner will match one integer grid square - allowing a 1:1 mapping of all points and rects into integer square grid.
    // ix and ix+xoff can be swapped (and iy, similarly), depending on starting point, so we need to sort them.
    int64_t grid_x = std::min(ix,ix+xoff);
    int64_t grid_y = std::min(iy,iy+yoff);
    
    return pointInt( grid_x, grid_y );
}

/******************************************************************************/
/******************************************************************************/
