//
//  main.cpp
//
//  Offgrid demonstration source
//      Not optimized, designed for readability
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <cstdio>
#include <cmath>
#include "hash.hpp"
#include "image.hpp"
#include "offgrid.hpp"
#include "offgridN.hpp"

/******************************************************************************/

// Used for documentation.
// Overlaying the checkerboard as a layer and decreasing opacity helps illustrate how the algorithm works
void CreateCheckerboard( const size_t width, const size_t height, const size_t box_size, const char *name )
{
    image imagebuf( width, height );

    int64_t h_count = (width + box_size-1)/box_size;
    int64_t v_count = (height + box_size-1)/box_size;
    //int64_t box_half = box_size / 2;
    
    for (int64_t y = 0; y <= v_count; ++y)
        {
        for (int64_t x = 0; x <= h_count; ++x)
            {
            // image is initialized to zero, so we only need to fill white squares
            if ( ((x^y) & 0x01) == 1)       // checkerboard pattern
                imagebuf.FillRectClipped( y*box_size, x*box_size, (y+1)*box_size, (x+1)*box_size, 255 );
            }
        }

    imagebuf.WritePGM( name );
}

/******************************************************************************/

// Iterate the integer grid to create irregular rectangle coordinates, and fill them.
// Create an image with hash values based on the integer grid.
void OffGridFillRect( const size_t width, const size_t height, const size_t box_size, const int64_t seed, const char *name )
{
    image imagebuf( width, height );
    
    int64_t boxes_wide = width / box_size;
    int64_t boxes_high = height / box_size;
    
    // We have to overscan the grid slightly because displaced rects will overlap edges.
    // ...and the scaled, centered coordinate system complicates things a little bit.
    for( int64_t y=-2; y <= boxes_high; ++y ) {
        for( int64_t x=-2; x <= boxes_wide; ++x ) {

            rectFloat theRect = Offgrid_CellToRect( x - boxes_wide/2, y - boxes_high/2, seed );

            // edge values will always match up between rects, because of the way they are calculated uniquely per edge
            // we just need to round consistently for them to draw correctly
            int64_t top = int64_t( floor(box_size*theRect.top) ) + height/2 + 1;
            int64_t left =  int64_t( floor(box_size*theRect.left) ) + width/2 + 1;
            int64_t bottom = int64_t( floor(box_size*theRect.bottom) ) + height/2 + 1;
            int64_t right = int64_t( floor(box_size*theRect.right) ) + width/2 + 1;

            int64_t cell_hash = hashXY( x - boxes_wide/2, y - boxes_high/2, seed );
            uint8_t pixel_value = uint8_t(cell_hash & 0xFF);

            imagebuf.FillRectClipped( top, left, bottom, right, pixel_value );
            
        }   // for width
    }   // for height
    
    imagebuf.WritePGM( name );
}

/******************************************************************************/

// Iterate the integer grid with N dimensional logic to create irregular rectangle coordinates, and fill them.
// Create an image with hash values based on the integer grid.
void OffGridFillRectN2( const size_t width, const size_t height, const size_t box_size, const int64_t seed, const char *name )
{
    image imagebuf( width, height );
    
    int64_t boxes_wide = width / box_size;
    int64_t boxes_high = height / box_size;
    
    // We have to overscan the grid slightly because displaced rects will overlap edges.
    // ...and the scaled, centered coordinate system complicates things a little bit.
    for( int64_t y=-2; y <= boxes_high; ++y ) {
        for( int64_t x=-2; x <= boxes_wide; ++x ) {
            
            vecN<int64_t,2> pt = { x - boxes_wide/2, y - boxes_high/2 };
            boxN<double,2> outBox = OffgridN_CellToBox2( pt, seed );

            // edge values will always match up between rects, because of the way they are calculated uniquely per edge
            // we just need to round consistently for them to draw correctly
            int64_t left =  int64_t( floor(box_size*outBox.min[0]) ) + width/2 + 1;
            int64_t top = int64_t( floor(box_size*outBox.min[1]) ) + height/2 + 1;
            int64_t right = int64_t( floor(box_size*outBox.max[0]) ) + width/2 + 1;
            int64_t bottom = int64_t( floor(box_size*outBox.max[1]) ) + height/2 + 1;

            int64_t cell_hash = hash64<2>( pt, seed );
            uint8_t pixel_value = uint8_t(cell_hash & 0xFF);

            imagebuf.FillRectClipped( top, left, bottom, right, pixel_value );
            
        }   // for width
    }   // for height
    
    imagebuf.WritePGM( name );
}

/******************************************************************************/

// Map from floating point grid (UI or texture) coordinates to integer grid.
// Create an image with hash values based on the integer grid.
void OffGridShader( const size_t width, const size_t height, const size_t box_size, const int64_t seed, const char *name )
{
    image imagebuf( width, height );
    
    double box_scale_v = double(height) / double(box_size);
    double box_scale_h = double(width) / double(box_size);
    
    for( int64_t y=0; y < height; ++y ) {
        double yy = box_scale_v * ((double(y)/double(height))-0.5);

        for( int64_t x=0; x < width; ++x ) {
            double xx = box_scale_h * ((double(x)/double(width))-0.5);

            // get a cell hash from the float coordinate
            pointInt grid_point = Offgrid_PointToCell( xx, yy, seed );
            
            int64_t cell_hash = hashXY( grid_point.x, grid_point.y, seed );

            // write value into buffer
            uint8_t pixel_value = uint8_t(cell_hash & 0xFF);
            imagebuf.WritePixel( x, y, pixel_value );
            
        }   // for width
    }   // for height

    imagebuf.WritePGM( name );
}

/******************************************************************************/

// Map from floating point (UI or texture) coordinates to integer grid using N dimensional logic
// Create an image with hash values based on the integer grid.
void OffGridShaderN2( const size_t width, const size_t height, const size_t box_size, const int64_t seed, const char *name )
{
    using vecInt = vecN<int64_t, 2>;
    using vecDouble = vecN<double, 2>;
    
    image imagebuf( width, height );
    
    double box_scale_v = double(height) / double(box_size);
    double box_scale_h = double(width) / double(box_size);
    
    for( int64_t y=0; y < height; ++y ) {
        double yy = box_scale_v * ((double(y)/double(height))-0.5);

        for( int64_t x=0; x < width; ++x ) {
            double xx = box_scale_h * ((double(x)/double(width))-0.5);

            // get a cell hash from the float coordinate
            vecDouble testPoint = { xx, yy };
            vecInt grid_point = OffgridN_PointToCell2( testPoint, seed );
            
            int64_t cell_hash = hash64<2>( grid_point, seed );
            
            // write value into buffer
            uint8_t pixel_value = uint8_t(cell_hash & 0xFF);
            imagebuf.WritePixel( x, y, pixel_value );
            
        }   // for width
    }   // for height

    imagebuf.WritePGM( name );
}

/******************************************************************************/
/******************************************************************************/

int main(int argc, const char * argv[])
{
    const int64_t shared_seed = hash64(0x8675309); // totally random number ;-)
    
    // the algorithm is not dependent on powers of 2
#if DOCUMENTATION
    const size_t width = 300;
    const size_t height = 300;
    const size_t box_size = (width/5);
#else
    const size_t width = 1000;
    const size_t height = 1000;
    const size_t box_size = (width/17);
#endif

    // Draw simple checkerboard for illustration of how the algorithm works.
    // Used for documentation.
    //CreateCheckerboard( width, height, box_size, "checkerboard.pgm" );

    // Draw offgrid with rect fill
    //      map integer grid -> irregular grid
    OffGridFillRect( width, height, box_size, shared_seed, "offgrid_fill.pgm" );

    // Draw offgrid with point proc/shader logic
    //      map arbitrary float / UI / shader coordinates -> integer grid
    OffGridShader( width, height, box_size, shared_seed, "offgrid_shader.pgm" );
    
    

    // Draw offgridN<2> with rect fill
    OffGridFillRectN2( width, height, box_size, shared_seed, "offgridN2_fill.pgm" );
    
    // Draw offgridN<2> with point proc/shader logic
    OffGridShaderN2( width, height, box_size, shared_seed, "offgridN2_shader.pgm" );

    // Draw offgridN<> with 3D boxes
    // Size limited to 5 because higher dimension files get huge, and Blender is slow.
    const size_t NDimCount = 5;
    OffgridN_Test1( NDimCount, shared_seed, "offgridN1.x3d" );
    OffgridN_Test2( NDimCount, shared_seed, "offgridN2.x3d" );

#if 0
// Well, crap.  More extensive testing shows gaps in the coverage for 3D and above.
// time to write more unit tests and figure out why.
    OffgridN_Test3( NDimCount, shared_seed, "offgridN3.x3d" );
    OffgridN_Test4( NDimCount, shared_seed, "offgridN4.x3d" );
    OffgridN_Test5( NDimCount, shared_seed, "offgridN5.x3d" );
    OffgridN_Test6( NDimCount, shared_seed, "offgridN6.x3d" );

    TestPointToCellN();
#endif

    
    return 0;
}

/******************************************************************************/
/******************************************************************************/
