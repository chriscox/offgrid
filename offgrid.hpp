//
//  offgrid.hpp
//  Offgrid demonstration source
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef offgrid_hpp
#define offgrid_hpp

#include <cstdint>
#include "image.hpp"

/******************************************************************************/

rectFloat Offgrid_CellToRect( int64_t ix, int64_t iy, int64_t seed );

pointInt Offgrid_PointToCell( double x, double y, int64_t seed );

/******************************************************************************/

#endif /* offgrid_hpp */
