//
//  image.hpp
//  Offgrid demonstration source
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef image_hpp
#define image_hpp

#include <cstdint>

/******************************************************************************/

struct pointInt
{
    pointInt( int64_t xx, int64_t yy ) :
        x(xx), y(yy) {}

    int64_t x, y;
};

/******************************************************************************/

struct pointFloat
{
    pointFloat( double xx, double yy ) :
        x(xx), y(yy) {}

    double x, y;
};

/******************************************************************************/

struct rectInt
{
    rectInt( int64_t t, int64_t l, int64_t b, int64_t r) :
        top(t), left(l), bottom(b), right(r) {}
    
    inline bool isValid()
        { return (top < bottom) && (left < right); }

    int64_t top, left, bottom, right;
};

/******************************************************************************/

struct rectFloat
{
    rectFloat( double t, double l, double b, double r) :
        top(t), left(l), bottom(b), right(r) {}
    
    // also fails for NaNs
    inline bool isValid()
        { return (top < bottom) && (left < right); }
    
    double top, left, bottom, right;
};

/******************************************************************************/

struct boxFloat
{
    boxFloat( double minX, double minY, double minZ, double maxX, double maxY, double maxZ ) :
        xmin(minX), ymin(minY), zmin(minZ), xmax(maxX), ymax(maxY), zmax(maxZ) {}
    
    boxFloat() : xmin(0.0), ymin(0.0), zmin(0.0), xmax(0.0), ymax(0.0), zmax(0.0) {}
    
    // also fails for NaNs
    inline bool isValid()
        { return (xmin < xmax) && (ymin < ymax) && (zmin < zmax); }
    
    double xmin, ymin, zmin, xmax, ymax, zmax;
};

/******************************************************************************/

struct point3Int
{
    point3Int( int64_t xx, int64_t yy, int64_t zz ) :
        x(xx), y(yy), z(zz) {}

    int64_t x, y, z;
};

/******************************************************************************/

struct point3Float
{
    point3Float( double xx, double yy, double zz ) :
        x(xx), y(yy), z(zz) {}

    double x, y, z;
};

/******************************************************************************/

inline
point3Int operator+(const point3Int &a, const point3Int &b)
{
    point3Int result = a;
    result.x += b.x;
    result.y += b.y;
    result.z += b.z;
    return result;
}

/******************************************************************************/

inline
point3Int operator+(const point3Int &a, int64_t b)
{
    point3Int result = a;
    result.x += b;
    result.y += b;
    result.z += b;
    return result;
}

/******************************************************************************/

inline
point3Int operator*(const point3Int &a, int64_t b)
{
    point3Int result = a;
    result.x *= b;
    result.y *= b;
    result.z *= b;
    return result;
}

/******************************************************************************/

// a minimal image buffer implementation, for demonstration purposes
struct image
{
    image() : width(0), height(0), buffer(NULL) {}
    
    image( size_t width, size_t height ) : buffer(NULL) {
        Allocate( width, height );
    }
    
    ~image() {
        width = height = 0;
        delete[] buffer;
        buffer = NULL;  // for debugging, and so memory reference leak checking doesn't get false positives
    }
    
    void Allocate( size_t width, size_t height );

    void WritePGM( const char *name );
    
    void WritePixel( int64_t x, int64_t y, uint8_t value );
    
    uint8_t ReadPixel( int64_t x, int64_t y );
    
    void FillRectClipped( int64_t top, int64_t left, int64_t bottom, int64_t right, uint8_t value );

private:
    size_t width;
    size_t height;
    uint8_t *buffer;
};

/******************************************************************************/

#endif /* image_hpp */
