//
//  hash.hpp
//  Offgrid demonstration source
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef hash_hpp
#define hash_hpp

#include <cstddef>
#include <cstdint>
#include <vector>
#include <array>

/******************************************************************************/

/// returns 0.0..1.0
double hash_to_double(const int64_t x);

/******************************************************************************/

/// simple but useful hash
int64_t hash64( const int64_t x );

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from x or y
int64_t hashXY( const int64_t x, const int64_t y, const int64_t seed );

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// returns 0.0..1.0
/// seed should be a hashed value (random bits) not derived from x or y
double hashXY_float( const int64_t x, const int64_t y, const int64_t seed );

/******************************************************************************/

/// Hash a list of coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from vector contents
int64_t hashList( const int64_t* vec, const size_t count, const int64_t seed );

/******************************************************************************/

/// Hash a list of coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from vector contents
inline int64_t hash64( const std::vector<int64_t> &vec, const int64_t seed )
{
    return hashList( &vec[0], vec.size(), seed );
}

/******************************************************************************/

/// Hash a list of coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from vector contents
template< size_t N >
inline int64_t hash64( const std::array<int64_t, N> &vec, const int64_t seed )
{
    return hashList( &vec[0], N, seed );
}

/******************************************************************************/

#endif /* hash_hpp */
