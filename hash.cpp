//
//  hash.cpp
//  Offgrid demonstration source
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include "hash.hpp"

/******************************************************************************/

/// returns 0.0..1.0
double hash_to_double(const int64_t x)
{
    const int BITS = 30;
    const uint64_t MASK = (1ULL << BITS)-1;
    const double scale = 1.0 / double(MASK);
    
    double result = (x & MASK) * scale;
    return result;
}

/******************************************************************************/

/// simple but useful hash that gets us reproducible values
/* based on crand64 from benchmark_algorithms.h */
int64_t hash64( const int64_t x )
{
    const uint64_t a = 6364136223846793005ULL;
    const uint64_t c = 1442695040888963407ULL;
    uint64_t temp = (x * a) + c;
    
    // without bit mixing the result is really bad, shows lots of periodicity
    temp = (temp >> 20) ^ (temp << 23) ^ temp;        // looks better
    return int64_t(temp);
}

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from x or y
int64_t hashXY( const int64_t x, const int64_t y, const int64_t seed )
{
    return hash64( hash64(x) ^ (y^seed) );
}

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// returns 0.0..1.0
/// seed should be a hashed value (random bits) not derived from x or y
double hashXY_float( const int64_t x, const int64_t y, const int64_t seed )
{
    int64_t temp = hashXY(x,y,seed);
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/

/// Hash a list of coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from vector contents
int64_t hashList( const int64_t* vec, const size_t count, const int64_t seed )
{
    int64_t result = seed;
    
    for (size_t i = 0; i < count; ++i)
        result = hash64( vec[i] ^ result );

    return result;
}

/******************************************************************************/
/******************************************************************************/
