# OffGrid
#

INCLUDE = -I.

CPPFLAGS = -std=c++17 $(INCLUDE) -O3

CPPLIBS = -lm

BINARIES = offgrid

all : $(BINARIES)

sources: main.cpp offgrid.cpp image.cpp hash.cpp offgridN.cpp 

headers: offgrid.hpp image.hpp hash.hpp offgridN.hpp

main.o: main.cpp $(headers)

offgrid.o: offgrid.cpp $(headers)

image.o: image.cpp $(headers)

hash.o: hash.cpp $(headers)

offgridN.o: offgridN.cpp $(headers)


offgrid: main.o offgrid.o image.o hash.o offgridN.o
	$(CXX) $(CPPFLAGS) $^ -o $@


test: all
	./offgrid

# declare some targets to be fakes without real dependencies
.PHONY : clean

# remove all the stuff we build
clean : 
		rm -f *.o $(BINARIES)
