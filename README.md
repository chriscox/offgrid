# OffGrid by Chris Cox

This code demonstrates a method of creating irregular looking rectangular/box grids, which still have a 1:1 mapping to a regular integer grid.
This can be used as a shader (greeble, game grid, etc.), for mapping UI coordinates back to the integer grid, or for drawing/creating an irregular grid by iterating an integer grid.


## Instructions
Download the source code, read the source code, compile on the command line, run, compare the generated images in any image editor.

## License
MIT License

![OffGrid example image](offgrid_example.png "OffGrid example image")
