//
//  offgridN.hpp
//      Offgrid demonstration source
//
//  Created by Chris Cox on July 8, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef offgridN_hpp
#define offgridN_hpp

#include <cstdint>
#include <array>
#include <vector>
#include <algorithm>
#include "image.hpp"

/******************************************************************************/

// N dimensional point/vector
template< typename T, size_t N >
using vecN = std::array<T, N>;

/******************************************************************************/

template< typename T, size_t N >
auto operator+( const vecN<T,N> &input, const vecN<T,N> &second )
{
    auto result = input;
    for (size_t i = 0; i < N; ++i)
        result[i] += second[i];
    return result;
}

/******************************************************************************/

// N dimensional box
template< typename T, size_t N >
struct boxN
{
    boxN()
        {
        std::fill(min.begin(),min.end(),T(0));
        std::fill(max.begin(),max.end(),T(0));
        }

    bool isValid() const
        {
        for (size_t i = 0; i < N; ++i)
            {
            if (max[i] <= min[i])
                return false;
            }
        return true;
        }
    
    bool ContainsPoint( const vecN<T,N> &pt )
        {
        for (size_t i = 0; i < N; ++i)
            {
            // half open so points on edge are only contained by one rect
            if ( (pt[i] < min[i]) || (pt[i] >= max[i]) )
                return false;
            }
        return true;
        }

    vecN<T,N> min;
    vecN<T,N> max;
};

/******************************************************************************/

template< typename T, size_t N >
vecN<int64_t, N> FloorPoint( const vecN<T, N> &pt )
{
    vecN<int64_t, N> result;
    for (size_t i = 0; i < N; ++i)
        result[i] = int64_t( floor( pt[i] ) );
    return result;
}

/******************************************************************************/

// convenience wrappers to output image
boxN<double,2> OffgridN_CellToBox2( const vecN<int64_t,2> &input, int64_t seed );
vecN<int64_t,2> OffgridN_PointToCell2( const vecN<double,2> &input, int64_t seed );

/******************************************************************************/

// debugging/unit test function
void TestPointToCellN();

/******************************************************************************/

void OffgridN_Test1( const size_t width, const int64_t seed, const char *name );
void OffgridN_Test2( const size_t width, const int64_t seed, const char *name );
void OffgridN_Test3( const size_t width, const int64_t seed, const char *name );
void OffgridN_Test4( const size_t width, const int64_t seed, const char *name );
void OffgridN_Test5( const size_t width, const int64_t seed, const char *name );
void OffgridN_Test6( const size_t width, const int64_t seed, const char *name );

/******************************************************************************/

#endif /* offgridN_hpp */
